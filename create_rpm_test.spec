Name            : msc-web
Summary         : msc-web
Version         : %{?version}%{!?version:1.0.0}
Release         : %{?release}%{!?release:0}

Group           : Applications/File
License         : (c)Douglas Gibbons

BuildArch       : %{?arch}%{!?arch:x86_64}
BuildRoot       : %{_tmppath}/%{name}-%{version}-root


# Use "Requires" for any dependencies, for example:
# Requires        : tomcat6

# Description gives information about the rpm package. This can be expanded up to multiple lines.
%description
Pome Web


# Prep is used to set up the environment for building the rpm package
# Expansion of source tar balls are done in this section
%prep
# Used to compile and to build the source
%build
# The installation.
# We actually just put all our install files into a directory structure that mimics a server directory structure here
%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT/msc_web
cp ../SOURCES/* $RPM_BUILD_ROOT/msc_web
#cp $RPM_BUILD_ROOT/msc_web /var/www/html



# Contains a list of the files that are part of the package
# See useful directives such as attr here: http://www.rpm.org/max-rpm-snapshot/s1-rpm-specref-files-list-directives.html
%files
%dir %attr(755, root, root) /msc_web
%attr(777, root, root) /msc_web/*


%post
cp -rf /msc_web/* /var/www/html


%preun


# Used to store any changes between versions
%changelog